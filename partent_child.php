<?php


class BITM
{
    public $Window; //property
    public $door;

    public function setWindow($local_Window) //method
    {
        $this->Window=$local_Window."[set by parent]";
    }

    public function show()
    {
        echo $this->Window."<br>";
    }
} // end of class

class BITM_402 extends BITM
{
    public function setWindowFromChild($Child_Window)
    {
        $Child_Window = $Child_Window."[set by child]";
        parent::setWindow($Child_Window);
    }
}

$myParentObject = new BITM();
$myParentObject->setWindow("i'm Window");
$myParentObject->show();

$myChildObject=new BITM_402();
$myChildObject->setWindow("this is a window");
$myChildObject->show();


$myChildObject->setWindowFromChild("ami ekti window");
echo $myChildObject->Window."<br>";
